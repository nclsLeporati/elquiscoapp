import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { ItemListPage } from '../pages/item-list/item-list';
import { ItemDetailPage } from '../pages/item-detail/item-detail';
import { PopoverItemList } from '../pages/item-list/pop-over';
import { CamPage } from '../pages/cam/cam';
import { ServicesListPage } from '../pages/services-list/services-list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { QRScanner } from '@ionic-native/qr-scanner';
//import { BarcodeScanner } from '@ionic-native/barcode-scanner';

import { JsonProvider } from '../providers/json/json';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    CamPage,
    ContactPage,
    HomePage,
    ItemListPage,
    ItemDetailPage,
    PopoverItemList,
    TabsPage,
    ServicesListPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    CamPage,
    ContactPage,
    HomePage,
    ItemListPage,
    ItemDetailPage,
    PopoverItemList,
    TabsPage,
    ServicesListPage
  ],
  providers: [
    QRScanner,
    PhotoViewer,
    EmailComposer,
    InAppBrowser,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    JsonProvider
  ]
})
export class AppModule {}
