import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { PopoverController, Content } from 'ionic-angular';

import { JsonProvider } from '../../providers/json/json';

import { ItemDetailPage } from '../item-detail/item-detail';
import { PopoverItemList } from './pop-over';

import { Item } from '../../models/item';

/**
 * Generated class for the ItemListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'item-list',
  templateUrl: 'item-list.html',
})
export class ItemListPage {
  @ViewChild(Content) content: Content;

  public category: string;
  public subCategories;
  public subCategory: string= "todos";
  public items: Item[] = [];
  public item: Item;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public jsonProv: JsonProvider,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController
  ){}

  ngOnInit(){
    this.category = this.navParams.get("category");
    this.subCategories = this.navParams.get("subCategories");
    console.log(this.subCategories);
    console.log(this.category);
    this.getItems(this.category);
  }

  clickItem(item){
    this.item = item;
    this.navCtrl.push(ItemDetailPage, {
      "item": this.item
    });
  }

  getItems(category){
    this.content.scrollTo(0, 0, 200);
    this.jsonProv.getJSON(category).subscribe(
      data => {
        console.log(data);
        if (this.subCategory !== "todos"){
          this.items = data.data
                                .filter((item: Item) => item.subCategory === this.subCategory)
                                .sort((lastOne, nextOne) => lastOne.name > nextOne.name ? 1 : -1);
        } else {
          this.items = data.data.sort((lastOne, nextOne) => lastOne.name > nextOne.name ? 1 : -1);
        }
        console.log(this.items);
      },
      error => {
        console.log("ERROR to get JSON");
      }
    );
  }

  showSubCategory() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Categoria');

    alert.addInput({
      type: 'radio',
      label: 'Todos',
      value: 'todos',
      checked: true
    });

    if (this.subCategories){
      this.subCategories.forEach(function (value) {
        alert.addInput({
          type: 'radio',
          label: value,
          value: value,
          checked: false
        });
      });
    };

    alert.addButton('Cancelar');
    alert.addButton({
      text: 'OK',
      handler: data => {
        //this.testRadioOpen = false;
        //this.testRadioResult = data;
        this.subCategory = data;
        this.getItems(this.category);
      }
    });
    alert.present();
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverItemList);
    popover.present({
      ev: myEvent
    });
  }

}
