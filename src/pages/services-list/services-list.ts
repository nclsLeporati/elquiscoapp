import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController  } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';

import { JsonProvider } from '../../providers/json/json';

//import { PopoverItemList } from './pop-over';

import { Item } from '../../models/item';

/**
 * Generated class for the ServicesListPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-services-list',
  templateUrl: 'services-list.html',
})
export class ServicesListPage {

  public category: string;
  public items: Item[] = [];
  public item: Item;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public jsonProv: JsonProvider,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController
  ){}

  ngOnInit(){
    this.category = "Servicios";
    this.getItems(this.category);
  }

  getItems(category){
    this.jsonProv.getJSON(category).subscribe(
      data => {
        console.log(data);
        this.items = data.data.sort((lastOne, nextOne) => lastOne.name > nextOne.name ? 1 : -1);
      },
      error => {
        console.log("ERROR to get JSON");
      }
    );
  }

  clickItem(phone: string){
    console.log(phone);
    window.location.href = "tel:"+phone;
  }

}
