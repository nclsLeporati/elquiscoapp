import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/**
 * Generated class for the CamPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cam',
  templateUrl: 'cam.html',
})
export class CamPage {

  constructor(
    public navCtrl: NavController,
    public qrScanner: QRScanner,
    private alertCtrl: AlertController,
    private iap: InAppBrowser
  ) {}

  ionViewDidEnter(){
    this.scan();
  }

  ionViewDidLeave(){
    this.qrScanner.hide();
    window.document.querySelector('ion-app').classList.remove('transparentBody')
  }

  scan(){
    window.document.querySelector('ion-app').classList.add('transparentBody')
    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
         if (status.authorized) {
           // camera permission was granted

           // start scanning
           let scanSub = this.qrScanner.scan().subscribe((text: string) => {
             let alert = this.alertCtrl.create({
                 title: 'Codigo QR',
                 subTitle: text,
                 buttons: [
                   {
                     text: 'Cancelar',
                     role: 'cancel'
                   },
                   {
                     text: 'Visitar',
                     handler: () => {
                       this.iap.create(text, "_system");
                     }
                   },
                 ]
                 });
             alert.present();

             this.qrScanner.hide(); // hide camera preview
             scanSub.unsubscribe(); // stop scanning
           });

           this.qrScanner.show();

         } else if (status.denied) {
           // camera permission was permanently denied
           // you must use QRScanner.openSettings() method to guide the user to the settings page
           // then they can grant the permission from there
           alert("Se necesita el permiso para acceder a la camara");
         } else {
           // permission was denied, but not permanently. You can ask for permission again at a later time.
           alert("Se necesita el permiso para acceder a la camara");
         }
      })
      .catch((e: any) => console.log('Error: ', e));
  }


}
