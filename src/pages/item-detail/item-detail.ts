import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { EmailComposer } from '@ionic-native/email-composer';
import { PhotoViewer } from '@ionic-native/photo-viewer';

import { Item } from '../../models/item';

/**
 * Generated class for the ItemDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
   selector: 'item-detail',
   templateUrl: 'item-detail.html',
 })
 export class ItemDetailPage {

   public item: Item;
   public stars: number[] = [];
   public showImages: boolean= false;

   constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     private iap: InAppBrowser,
     public emailComposer: EmailComposer,
     public photoViewer: PhotoViewer
   ) {}

   ngOnInit(){
     this.item = this.navParams.get('item');
     for (let i=0; i < this.item.stars; i++){
       this.stars.push(1);
     }
     console.log(this.item);     
     document.getElementById("item-body").innerHTML = this.item.body;
     if (this.item.equips != null)    
      document.getElementById("item-equips").innerHTML = this.item.equips;
     if (this.item.services != null)
      document.getElementById("item-services").innerHTML = this.item.services;
   }

   goWeb(url: string){
    this.iap.create(url, "_system");
   }

   goApp(app: string, cont: string){
     //  window.location.href="market://search?q=pub:Spotify";
     console.log(app+":"+cont);
     window.location.href = app+":"+cont;
   }

   goGeo(cont: string){
     window.location.href = "geo:"+cont+"&zoom=5";
   }

   popPage(){
     this.navCtrl.pop();
   }

   sendEmail(){
     let email = {
       to: this.item.email,
       cc: '',
       subject: 'ElQuiscoApp - '+this.item.name,
       body: 'Contacto generado por ElQuiscoApp',
       isHtml: true
     };
     this.emailComposer.open(email);
   }

   viewPhoto(){
     console.log(this.item.img);
     this.showImages = true;
     this.photoViewer.show(this.item.img, this.item.name, {share: true});
   }


 }
