import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ItemListPage } from '../item-list/item-list';
import { ServicesListPage } from '../services-list/services-list';

import { JsonProvider } from '../../providers/json/json';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public category: string;
  public subCategories;

  constructor(
    public navCtrl: NavController,
    public jsonProv: JsonProvider
  ) {}

  ngOnInit(){
    //this.getSubCategories("Sub_Alojamientos");
  }

  clickItemList(category){
    this.category = category;
    let categories = "Sub_"+category;
    this.getSubCategories(categories);
    console.log(this.subCategories);
    this.toItem();
  }

  toItem(){
    this.navCtrl.push(ItemListPage, {
      "category": this.category,
      "subCategories": this.subCategories
    });
  }

  getSubCategories(subCategories){
    this.jsonProv.getJSON(subCategories).subscribe(
      data => {
        this.subCategories = data.data;
        console.log(this.subCategories);
      },
      error => {
        console.log("ERROR to get JSON");
      }
    );
  }

  clickServicesList(){
    this.navCtrl.push(ServicesListPage);
  }

}
