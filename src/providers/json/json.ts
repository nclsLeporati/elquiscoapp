import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";

@Injectable()
export class JsonProvider {

  constructor(private http: HttpClient) {}

  public getJSON(db: string): Observable<any> {
    return this.http.get("./database/" + db + ".json")
  }
}
