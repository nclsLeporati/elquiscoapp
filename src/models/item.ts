export class Item{
  constructor(
    public id: string,
    public name: string,
    public text: string,
    public body: string,
    public equips: string,
    public services: string,
    public img: string,
    public imgs: string[],
    public stars: number,
    public direction: string,
    public geo: string,
    public phone: number,
    public email: string,
    public web: string,
    public category: string,
    public subCategory: string,
    public favs: number,
    public comments: number,
  ){}
}
